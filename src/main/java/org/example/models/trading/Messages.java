package org.example.models.trading;

public class Messages {
  public static class BuyOrderPlaced {}

  public static class SellOrderPlaced {}
}
